# [Acatenango - v1.1 (2022-01-21)](https://gitlab.com/dlr-ve/esy/amiris/amiris/-/releases/v1.1): 
Harmonised contracting for Forecaster and Exchange

# [Acamarachi - v1.0 (2021-12-06)](https://gitlab.com/dlr-ve/esy/amiris/amiris/-/releases/v1.0): 
Initial release of AMIRIS
